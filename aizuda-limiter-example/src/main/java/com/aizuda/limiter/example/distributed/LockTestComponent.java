package com.aizuda.limiter.example.distributed;

import com.aizuda.limiter.annotation.DistributedLock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @author zhongjiahua
 * @date 2021-11-28
 * @Description: 分布式锁测试
 */
@Slf4j
@Component
public class LockTestComponent {

    @DistributedLock(
            // 唯一标示，支持SpEL表达式（可无），'#name!=null?#name:'default'' 为获取当前访问参数 name 内容，如果为空，则默认为 'default'
            key = "#name!=null?#name:'default'",
            // 指定时间获取锁，如果在指定时间获取不到，则抛出对应异常，捕获此异常即可
            tryAcquireTimeout = "20s"
    )
    public void testLockListener(String name) {
        log.debug("Thread:{} execute testLockListener method",Thread.currentThread().getId());
        try {
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
    }
}
