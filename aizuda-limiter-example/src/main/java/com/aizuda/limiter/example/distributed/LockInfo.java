package com.aizuda.limiter.example.distributed;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author zhongjiahua
 * @date 2021-12-06
 * @Description:
 */
@Setter
@Getter
@ToString
@Accessors(chain = true)
public class LockInfo {
    private String lockKey;
    private boolean acquireLockSuccessBool;
    private boolean executeMethodSuccessBool;
}
