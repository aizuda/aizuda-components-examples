package com.aizuda.limiter.example.distributed;

import com.aizuda.limiter.annotation.DistributedLock;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * @author zhongjiahua
 * @date 2021-11-28
 * @Description: 测试分布式锁方式
 */
@RestController
@RequiredArgsConstructor
@Slf4j
public class LockTestController {
    private final LockTestComponent lockTestComponent;
    private final ApplicationContext applicationContext;
    private boolean alreadyReentrant;

    /**
     * 测试分布式锁的接口，建议多开几个实例测试、
     * 支持可重如
     * --server.port=8081
     *
     * @param name 参数
     * @return map
     */
    @GetMapping("/lock-reentrant")
    @DistributedLock(
            // 唯一标示，支持SpEL表达式（可无），'#name!=null?#name:'default'' 为获取当前访问参数 name 内容，如果为空，则默认为 'default'
            key = "#name!=null?#name:'default'",
            // 指定时间获取锁，如果在指定时间获取不到，则抛出对应异常，捕获此异常即可
            tryAcquireTimeout = "5s",
            acquireTimeoutMessage = "您请求的太快了"
    )
    public Map<String, Object> testLock(String name) {
        log.debug("Thread:{} execute first method", Thread.currentThread().getId());
        // 测试可重入
        LockTestController bean = applicationContext.getBean(LockTestController.class);
        if (!alreadyReentrant) {
            alreadyReentrant = true;
            bean.testLock(null);
            log.info("运行到这里，说明此锁是可重入的");
        }
        try {
            TimeUnit.SECONDS.sleep(20);
        } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }

        return new HashMap<String, Object>(4) {{
            put("name", Optional.ofNullable(name).orElse("default string"));
        }};
    }

    /**
     * 测试获取分布式锁超时策略
     *
     * @param name 参数
     * @return map
     */
    @GetMapping("/failed-handler-test")
    @DistributedLock(
            // 唯一标示，支持SpEL表达式（可无），'#name!=null?#name:'default'' 为获取当前访问参数 name 内容，如果为空，则默认为 'default'
            key = "#name!=null?#name:'default'",
            // 指定时间获取锁，如果在指定时间获取不到，则抛出对应异常，捕获此异常即可
            tryAcquireTimeout = "5s"
    )
    public Map<String, Object> testFailedHandler(String name) {
        try {
            TimeUnit.SECONDS.sleep(20);
        } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }

        return new HashMap<String, Object>(4) {{
            put("name", Optional.ofNullable(name).orElse("default string"));
        }};
    }

    /**
     * 测试监听器
     *
     * @param name 参数
     * @return map
     */
    @GetMapping("/lock-listener-test")
    @DistributedLock(
            // 唯一标示，支持SpEL表达式（可无），'#name!=null?#name:'default'' 为获取当前访问参数 name 内容，如果为空，则默认为 'default'
            key = "#name!=null?#name:'default'",
            // 指定时间获取锁，如果在指定时间获取不到，则抛出对应异常，捕获此异常即可
            tryAcquireTimeout = "5s"
    )
    public Map<String, Object> testLockListener(String name) {
        lockTestComponent.testLockListener(name);
        return new HashMap<String, Object>(4) {{
            put("name", Optional.ofNullable(name).orElse("default string"));
        }};
    }
}
