package com.aizuda.limiter.example.distributed;

import com.aizuda.limiter.extend.IAcquireLockTimeoutHandler;
import com.aizuda.limiter.metadata.MethodMetadata;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;

/**
 * @author zhongjiahua
 * @date 2021-12-06
 * @Description: 自定义失败处理策略
 */
@Component
public class ControllerAcquireLockTimeoutHandler implements IAcquireLockTimeoutHandler {
    @Override
    public boolean supports(MethodMetadata methodMetadata) {
        return AnnotatedElementUtils.hasAnnotation(methodMetadata.getMethod(), RequestMapping.class)
                && "com.aizuda.limiter.example.distributed.LockTestController.testFailedHandler".equals(methodMetadata.getClassMethodName());
    }

    @Override
    public Object onDistributedLockFailure(MethodMetadata methodMetadata) {
        return new HashMap<String, Object>(4) {{
            put("failedMessage", "failed acquired lock");
        }};
    }

}
