package com.aizuda.limiter.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 限流模块示例
 *
 * @author 青苗
 * @since 2021-11-21
 */
@SpringBootApplication
public class LimiterExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(LimiterExampleApplication.class, args);
    }
}
