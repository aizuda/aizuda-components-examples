package com.aizuda.limiter.example;

import com.aizuda.limiter.metadata.MethodMetadata;
import com.aizuda.limiter.strategy.IKeyGenerateStrategy;
import org.springframework.stereotype.Component;

@Component
public class UserRateLimitStrategy implements IKeyGenerateStrategy {
    public final static String TYPE = "user";


    @Override
    public String getType() {
        // 请保证唯一性
        return TYPE;
    }

    @Override
    public String getKey(MethodMetadata methodMetadata, String parseKey) {
        return "admin";
    }
}
