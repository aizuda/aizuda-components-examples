
# aizuda-components

![logo](https://portrait.gitee.com/uploads/avatars/namespace/2879/8637007_aizuda_1636162864.png!avatar100)

> 开发平台不允许做非法网站，后果自负

- 爱组搭 ~ 低代码组件化开发平台之组件库

- 愿景：每个人都是架构师

[爱组搭 ~ 组件源码](https://gitee.com/aizuda/aizuda-components)

# 模块介绍

- aizuda-common-example 公共模块示例

- aizuda-limiter-example 限流模块示例

- aizuda-robot-example 机器人模块示例

- aizuda-security-example 安全模块示例

- aizuda-oss-example 文件存储模块示例




