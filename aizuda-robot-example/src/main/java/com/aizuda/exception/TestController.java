package com.aizuda.exception;

import com.aizuda.robot.exception.ISendException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class TestController {
    @Resource
    private ISendException sendException;

    /**
     * http://localhost:8080/test
     * http://localhost:8080/test?param=0
     * http://localhost:8080/test?param=100
     * http://localhost:8080/test?param=123
     */
    @GetMapping("test")
    public Object test(Integer param) {
        //这里测试空指针
        if (null == param) {
            return param.intValue();
        }
        //这里测试除以0的异常
        if (0 == param) {
            return 1 / param;
        }
        if (100 == param) {
            return "运行成功";
        }
        throw new RuntimeException("运行时异常");
    }


    @GetMapping("send")
    public boolean send() {
        return sendException.send("手动发送一个异常");
    }
}
