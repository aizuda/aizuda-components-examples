package com.aizuda.exception;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.OkHttp3ClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class RobotExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(RobotExampleApplication.class, args);
    }

    /**
     * 基于OkHttp3配置RestTemplate
     * <p>
     * 自定义 RestTemplate 可不配置
     */
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate(new OkHttp3ClientHttpRequestFactory());
    }
}
