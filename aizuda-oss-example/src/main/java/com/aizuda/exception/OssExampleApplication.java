package com.aizuda.exception;

import com.aizuda.oss.IFileStorage;
import com.aizuda.oss.autoconfigure.OssProperty;
import com.aizuda.oss.model.StoragePlatform;
import com.aizuda.oss.platform.Minio;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class OssExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(OssExampleApplication.class, args);
    }

    @Bean
    public IFileStorage minio3() {
        // 注入一个自定义存储平台
        OssProperty ossProperty = new OssProperty();
        ossProperty.setPlatform(StoragePlatform.minio);
        ossProperty.setBucketName("test3");
        ossProperty.setEndpoint("http://127.0.0.1:9019");
        ossProperty.setAccessKey("q7RNi6elbvQ0j1ry");
        ossProperty.setSecretKey("HMoKkeu0zGSvSdDGWlMDuytaRON12St9");
        return new Minio(ossProperty);
    }
}
