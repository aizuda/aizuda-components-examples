package com.aizuda.monitor.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 服务器监控模块示例
 *
 * @author 青苗
 * @since 2022-03-02
 */
@SpringBootApplication
public class MonitorExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(MonitorExampleApplication.class, args);
    }
}
