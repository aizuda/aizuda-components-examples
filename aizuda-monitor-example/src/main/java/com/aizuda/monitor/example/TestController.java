/*
 * 爱组搭，低代码组件化开发平台
 * ------------------------------------------
 * 受知识产权保护，请勿删除版权申明，开发平台不允许做非法网站，后果自负
 */
package com.aizuda.monitor.example;

import com.aizuda.monitor.OshiMonitor;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import oshi.hardware.*;
import oshi.software.os.NetworkParams;
import oshi.software.os.OSProcess;
import oshi.software.os.OperatingSystem;
import oshi.util.FormatUtil;

import java.util.*;

/**
 * 基于 OShi 服务器信息收集监控
 * <p>
 * 尊重知识产权，CV 请保留版权，开发平台不允许做非法网站，后果自负
 *
 * @author 青苗
 * @since 2022-03-02
 */
@RestController
public class TestController {
    @Resource
    private OshiMonitor oshiMonitor;

    /**
     * 服务器信息收集
     * <p>
     * http://localhost:8080/test
     */
    @GetMapping("/test")
    public Map<String, Object> test(String name) {
        Map<String, Object> monitor = new HashMap<>(5);
        monitor.put("sysInfo", oshiMonitor.getSysInfo());
        monitor.put("cupInfo", oshiMonitor.getCpuInfo());
        monitor.put("memoryInfo", oshiMonitor.getMemoryInfo());
        monitor.put("jvmInfo", oshiMonitor.getJvmInfo());
        monitor.put("diskInfos", oshiMonitor.getDiskInfos());
        monitor.put("netIoInfo", oshiMonitor.getNetIoInfo());

        // 操作系统信息
        ComputerSystem computerSystem = oshiMonitor.getComputerSystem();
        final Firmware firmware = computerSystem.getFirmware();
        final Baseboard baseboard = computerSystem.getBaseboard();
        monitor.put("computerSystem", new HashMap<String, Object>() {{
            put("manufacturer", computerSystem.getManufacturer());
            put("model", computerSystem.getModel());
            put("serialNumber", computerSystem.getSerialNumber());
            put("firmware-manufacturer", firmware.getManufacturer());
            put("firmware-name", firmware.getName());
            put("firmware-description", firmware.getDescription());
            put("firmware-version", firmware.getVersion());
            put("baseboard-manufacturer", baseboard.getManufacturer());
            put("baseboard-model", baseboard.getModel());
            put("baseboard-version", baseboard.getVersion());
            put("baseboard-serialNumber", baseboard.getSerialNumber());
        }});

        // 系统前 10 个进程
        List<OSProcess> processList = oshiMonitor.getWindowsOperatingSystem().getProcesses(null,
                OperatingSystem.ProcessSorting.CPU_DESC, 10);
        List<String> strList = new ArrayList<>();
        for (OSProcess process : processList) {
            // 进程名，进程ID，进程CPU使用率
            strList.add(String.format("name:%s PID: %d CPU:%.3f", process.getName(), process.getProcessID(),
                    process.getProcessCpuLoadCumulative()));
        }
        monitor.put("processList", strList);

        HardwareAbstractionLayer hardware = oshiMonitor.getHardwareAbstractionLayer();
        printNetworkInterfaces(hardware.getNetworkIFs());
        OperatingSystem operatingSystem = oshiMonitor.getOperatingSystem();
        printNetworkParameters(operatingSystem.getNetworkParams());
        printUsbDevices(hardware.getUsbDevices(true));
        printSensors(hardware.getSensors());
        printPowerSources(hardware.getPowerSources());
        printDisplay(hardware.getDisplays());
        return monitor;
    }

    public static void printUsbDevices(List<UsbDevice> usbDeviceList) {
        usbDeviceList.forEach((item) -> System.out.println("Usb驱动：" + item.toString()));
    }

    private static void printNetworkInterfaces(List<NetworkIF> networkIFs) {
        networkIFs.forEach((item) -> System.out.println(String.format("网络设置，名称: %s ，MAC: %s ，MTU: %s ，网速bps: %s ，IPv4: %s ，IPv6: %s",
                item.getName(), item.getMacaddr(), item.getMTU(), FormatUtil.formatValue(item.getSpeed(), "bps"),
                Arrays.toString(item.getIPv4addr()), Arrays.toString(item.getIPv6addr()))));
    }

    private static void printNetworkParameters(NetworkParams networkParams) {
        System.out.println(String.format("网络，Host: %s ，Domain: %s ，DNS: %s ，IPv4: %s ，IPv6: %s", networkParams.getHostName(),
                networkParams.getDomainName(), Arrays.toString(networkParams.getDnsServers()), networkParams.getIpv4DefaultGateway()
                , networkParams.getIpv6DefaultGateway()));
    }

    private static void printSensors(Sensors sensors) {
        System.out.println(String.format("传感器，CPU温度: %s ，风扇: %s ，CPU电压: %s", sensors.getCpuTemperature(),
                Arrays.toString(sensors.getFanSpeeds()), sensors.getCpuVoltage()));
    }

    private static void printPowerSources(List<PowerSource> powerSources) {
        powerSources.stream().forEach(item -> System.out.println(String.format("电源，电源名称: %s ，剩余百分比: %s",
                item.getName(), item.getRemainingCapacityPercent() * 100)));
    }

    private static void printDisplay(List<Display> displays) {
        displays.stream().forEach(item -> System.out.println(String.format("显示，显示: %s", item.toString())));
    }
}
