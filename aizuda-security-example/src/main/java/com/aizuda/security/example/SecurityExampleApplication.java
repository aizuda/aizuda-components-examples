package com.aizuda.security.example;

import com.aizuda.security.autoconfigure.SecurityProperties;
import com.aizuda.security.request.ParamsSignRequestFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

/**
 * 测试文件 resources/ApiTest.http
 * <p>
 * 尊重知识产权，CV 请保留版权，开发平台不允许做非法网站，后果自负
 *
 * @author 青苗
 */
@SpringBootApplication
public class SecurityExampleApplication {

    /**
     * 注入请求参数签名过滤器
     */
    @Bean
    public FilterRegistrationBean paramsSignRequestFilter(SecurityProperties securityProperties) {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(new ParamsSignRequestFilter(securityProperties));
        registrationBean.addUrlPatterns("/*");
        return registrationBean;
    }

    /**
     * 测试文件 resources/ApiTest.http
     */
    public static void main(String[] args) {
        SpringApplication.run(SecurityExampleApplication.class, args);
    }
}

