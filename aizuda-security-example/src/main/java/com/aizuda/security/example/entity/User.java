package com.aizuda.security.example.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 无需加密处理对象
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private Long id;
    private String username;
    private String password;
    private String email;

    public static User newUser(LoginParam loginParam) {
        return new User(0L, loginParam.getUsername(), loginParam.getPassword(), "jobob@qq.com");
    }
}
