package com.aizuda.security.example.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * 无需加密处理对象
 */
@Getter
@Setter
public class LoginParam {
    private String username;
    private String password;

}
