package com.aizuda.security.example.controller;

import com.aizuda.security.example.entity.LoginParamDTO;
import com.aizuda.security.example.entity.User;
import com.aizuda.security.example.entity.UserVO;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试文件 resources/ApiTest.http
 * <p>
 * 尊重知识产权，CV 请保留版权，开发平台不允许做非法网站，后果自负
 */
@RestController
@AllArgsConstructor
public class SecurityController {

    /**
     * GET 验证参数签名，请求为加密对象接收参数，非 json 格式不会执行解密逻辑
     * <p>
     * url 传参解密逻辑无效，会加密响应
     */
    @GetMapping("/signGet")
    public User signGet(LoginParamDTO dto) {
        return User.newUser(dto);
    }

    /**
     * POST 验证参数 签名 忽略加密解密逻辑
     * <p>
     * sn 不会作为签名内容
     * </p>
     */
    @PostMapping("/signPost")
    public User signPost(@RequestBody User user, String sn) {
        System.err.println("----signPost----" + sn);
        return user;
    }

    /**
     * POST 验证参数 签名 + 解密 返回 加密
     * <p>
     * sn 不会作为签名内容
     * </p>
     */
    @PostMapping("/signRsaPost")
    public UserVO signRsaPost(@RequestBody LoginParamDTO dto, String sn) {
        System.err.println("----signRsaPost----" + sn);
        return UserVO.newUserVO(dto);
    }

    /**
     * POST 验证参数 签名 + 解密 返回不加密
     * <p>
     * sn 不会作为签名内容
     * </p>
     */
    @PostMapping("/signNoRsaPost")
    public User signNoRsaPost(@RequestBody LoginParamDTO dto, String sn) {
        System.err.println("----signNoRsaPost----" + sn);
        return User.newUser(dto);
    }
}
