package com.aizuda.security.example;

import com.aizuda.security.exception.ParamsSignException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.crypto.BadPaddingException;

@ControllerAdvice
public class TestExceptionHandler {

    /**
     * 全局捕获限流器异常处理
     */
    @ExceptionHandler(value = ParamsSignException.class)
    @ResponseBody
    public String exceptionHandler(ParamsSignException e) {
        return e.getMessage();
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public String badPaddingExceptionHandler(Exception e) {
        return "系统异常：" + e.getMessage();
    }
}
