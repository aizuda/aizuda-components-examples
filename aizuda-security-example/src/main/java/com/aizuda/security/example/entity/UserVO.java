package com.aizuda.security.example.entity;

import com.aizuda.security.annotation.Encrypted;

/**
 * 加密处理对象
 */
public class UserVO extends User implements Encrypted {

    public static UserVO newUserVO(LoginParamDTO dto) {
        UserVO vo = new UserVO();
        vo.setId(1L);
        vo.setUsername(dto.getUsername());
        vo.setPassword(dto.getPassword());
        vo.setEmail("hi@aizuda.com");
        return vo;
    }
}
