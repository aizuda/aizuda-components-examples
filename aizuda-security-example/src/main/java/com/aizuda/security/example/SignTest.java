package com.aizuda.security.example;

import com.aizuda.common.toolkit.JacksonUtils;
import com.baomidou.kisso.common.encrypt.MD5;
import com.baomidou.kisso.common.encrypt.RSA;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;
import java.util.SortedMap;
import java.util.TreeMap;

public class SignTest {

    public static void main(String[] args) throws Exception {
        SortedMap<String, String> parameterMap = new TreeMap<>();
        parameterMap.put("username", "test");
        parameterMap.put("password", "123");
        String timestamp = String.valueOf(System.currentTimeMillis());
        String jsonStr = JacksonUtils.toJSONString(parameterMap);
        String jsonMd5 = MD5.toMD5(jsonStr);

        // md5(md5(内容) + 时间戳) = sign
        String sign = MD5.toMD5(jsonMd5 + timestamp);
        System.err.println("ts=" + timestamp + "&sn=" + sign);

        // RSA 私钥加密 + base64 + MD5 签名
        String rsaBase64 = Base64.encodeBase64String(RSA.encryptByPrivateKey(jsonStr.getBytes(StandardCharsets.UTF_8),
                "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAIyUbRPT9K9B0NbVS+bcJAhN2j/H+yv4AN2cQVBaNO4kWAC4wi+TU6pRAOvQPnxutBZuR2OOLWmqzE1/tTd77o2LUZzXQVq4gpAFzBFEaf9XcCP7XruG0XRi3H3MBGD/kLr3cUea1zvpx+wHgnTdauKCLP5KvW85JttDk2hIsrF9AgMBAAECgYArmr5asBvtYJTOjkqEyTPD/6H6tqUJ9lsOYjFAIzrXwx4o2yYga6o/4fBPNMCtCAmEC6DJGIithfTo9PvYwfA0L0UQMetmltuNitU2Jk4t4zGYcLohPsHU8cfRarFRsOcHbWvC3xLNDMrjQGwt99XM9Gz84Ba6P9acGVmllmCoYQJBAOdzGhiv5eN/BJXyuroCb70uUeHWrDCpHdKOe7Yr6kgr8+3uyQrzqUTdOfcEfUHRDFu5igObgfUPyzrEqin8dbMCQQCbfcy0wBielUVwBArvgDD7r8qdeojtiuxuSvII6ld05B5UUz1xfhFj1aOD2vpHBq87yPj/28bmLrBTFkl0KwQPAkByOquhjYa/XdwMiYzA6xzs2KSO+p1nMsBGQA4sncX2MsMkJhrRLerX0vudv3h2eE9KIetM1n4wTcg2zANTz4bjAj8dJ3k6asoy8coTNq7WOTL1/f//hvolj+bEx0iaY1YClMMbDIB4xyp7jZMbJfPHXXl9vDDzyEGC5z4oVgVvwqECQQCVeDfrjp1VELpaokeAryMqxZ0AFIVcOXFGfNrFLfw3hJHNmd888sODtH91wJoluVQ3GDpMaPoF38IY9RScDv4v"));
        System.err.println("密文：\n" + rsaBase64);
        String signRsa = DigestUtils.md5DigestAsHex((MD5.toMD5(rsaBase64) + timestamp).getBytes(StandardCharsets.UTF_8));
        System.err.println("ts=" + timestamp + "&sn=" + signRsa);
    }
}
